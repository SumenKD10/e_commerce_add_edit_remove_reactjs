/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import { Link } from "react-router-dom";

class EachItem extends Component {
  handleClickDelete = (id) => {
    this.props.removeItem(id);
  };

  render() {
    let { id, category, image, title, description, price, rating } =
      this.props.eachItemDetail;
    return (
      <div>
        <div className="itemCard">
          <p className="category">📍{category}</p>
          <img className="eachItemImage" src={image} alt="No Image Available" />
          <div className="ratingsAndCount">
            <p>⭐{rating.rate}</p>
            <p>👤{rating.count}</p>
          </div>
          <div className="card-body">
            <h5 className="card-title">{title}</h5>
            <p className="card-text">{description}</p>
          </div>
          <div className="card-footer">
            <small className="card-price">${price}</small>
            <Link to={`/UpdateItem/${id}`}>
              <i
                className="fa-solid fa-square-pen"
                onClick={() => {
                  return this.handleClickUpdate;
                }}
              ></i>
            </Link>
            <i
              className="fa-sharp fa-solid fa-trash"
              onClick={() => {
                return this.handleClickDelete(id);
              }}
            ></i>
          </div>
        </div>
      </div>
    );
  }
}

export default EachItem;
