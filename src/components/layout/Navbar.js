import React, { Component } from "react";
import { Link } from "react-router-dom";

class Navbar extends Component {
  render() {
    return (
      <nav>
        <Link to="/">
          <h1>Do-Shop</h1>
        </Link>
        <Link to="/AddItem">
          <input type="button" value="Add Item" className="addButton"></input>
        </Link>
      </nav>
    );
  }
}

export default Navbar;
