import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <footer>
        <h1>Do-Shop</h1>
        <p>All rights reserved © Sumen Kumar Dubey</p>
        <div>
          <h4>Contacts</h4>
          <p>7XNCNJNJC8</p>
        </div>
      </footer>
    );
  }
}

export default Footer;
