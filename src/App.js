//Importing npm packages
import axios from "axios";
import React, { Component } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

//Importing other files
import "./App.css";
import Product from "./components/Product.js";
import ProductAddForm from "./components/ProductAddForm.js";
import ProductUpdateForm from "./components/ProductUpdateForm.js";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      image: "",
      price: "",
      description: "",
      id: "",
      dataRecieved: [],
      API_STATUS: "LOADING",
    };
  }

  componentDidMount() {
    //To Fetch all the data from API
    axios
      .get("https://fakestoreapi.com/products/")
      .then((dataGot) => {
        this.setState({
          ...this.state,
          API_STATUS: "LOADED",
          dataRecieved: dataGot.data,
        });
      })
      .catch((errorMessage) => {
        this.setState({ API_STATUS: "ERROR" });
      });
  }

  //Remove an Item
  removeItem = (idGot) => {
    this.setState({
      dataRecieved: this.state.dataRecieved.filter((eachData) => {
        return eachData.id !== idGot;
      }),
    });
  };

  //Add an Item
  addItem = (oneItem, newId) => {
    const newProduct = {
      ...oneItem,
      id: newId,
      rating: {
        rate: 0,
        count: 0,
      },
    };

    const newProducts = [...this.state.dataRecieved, newProduct];
    this.setState({
      dataRecieved: newProducts,
    });
  };

  //Update an Item
  updateItemClicked = (oneItem) => {
    const newProductsFiltering = this.state.dataRecieved.filter((eachData) => {
      return eachData.id.toString() !== oneItem.id.toString();
    });
    const newProducts = [oneItem, ...newProductsFiltering];
    this.setState({
      dataRecieved: newProducts,
    });
  };

  render() {
    // Making all the Routes and sending props
    return (
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Product
                API_STATUS={this.state.API_STATUS}
                dataRecieved={this.state.dataRecieved}
                removeItem={this.removeItem}
                updateItem={this.updateItemClicked}
              />
            }
          />
          <Route
            path="/AddItem"
            element={<ProductAddForm addItem={this.addItem} />}
          />
          <Route
            path="/UpdateItem/:productId"
            element={
              <ProductUpdateForm
                updateItem={this.updateItemClicked}
                allProducts={this.state.dataRecieved}
              />
            }
          />
        </Routes>
      </BrowserRouter>
    );
  }
}

export default App;
